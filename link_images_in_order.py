import os
import subprocess


def _main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("outfolder")
    parser.add_argument("infiles", nargs="+")
    args = parser.parse_args()

    os.makedirs(args.outfolder, exist_ok=True)

    for i, fn in enumerate(args.infiles, 1):
        subprocess.check_call(["ln", "-s",
                               os.path.abspath(fn),
                               os.path.join(args.outfolder, f"{i:05d}.png")])


if __name__ == "__main__":
    _main()
